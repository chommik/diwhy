PROJECT = $(error PROJECT is not set)
VARIANT = $(error VARIANT is not set)
BOARD = $(error BOARD is not set)

ROOT = $(shell pwd)
PROJECT_ROOT = $(ROOT)/projects/$(PROJECT)

DIR_OUT = $(ROOT)/build
DIR_EXT = $(ROOT)/ext
DIR_LIB = $(ROOT)/lib
DIR_DIST = $(ROOT)/dist

include make/verbose.mk
include make/flags.mk

include make/board/$(BOARD).mk
include make/variant/$(VARIANT).mk
include make/target/$(TARGET).mk

include $(PROJECT_ROOT)/project.mk

DIR_BIN = $(DIR_OUT)/$(PROJECT)/$(TARGET)/$(BOARD)
DIR_BIN_PROJECT = $(DIR_BIN)/project
DIR_BIN_PLATFORM = $(DIR_BIN)/platform
DIR_BIN_DEPS = $(DIR_BIN)/deps
FLASH_SYMS = $(DIR_BIN)/flash.sym
FLASH_IMAGE = $(DIR_BIN)/flash.img
ELF_IMAGE = $(DIR_BIN)/flash.elf

BASIC_DIRS = $(DIR_OUT) \
			 $(DIR_BIN) \
			 $(DIR_BIN_PROJECT) \
			 $(DIR_BIN_PLATFORM) \
			 $(DIR_BIN_DEPS) \

# -------------------------------------------------------------
#  Convenient aliases
# -------------------------------------------------------------

flash: $(FLASH_IMAGE)
.PHONY: flash

elf: $(ELF_IMAGE)
.PHONY: elf

# -------------------------------------------------------------
#  DIR_OUT setup
# -------------------------------------------------------------

%: | $(DIR_OUT)

%: $(BASIC_DIRS)
$(BASIC_DIRS):
	@echo $(MSG_MKDIR)
	$(Q)mkdir -p $@

# -------------------------------------------------------------
#  Optionally print details
# -------------------------------------------------------------

ifeq ($(D),1)
    $(info -----------------------------)
    $(info Compiling for:)
    $(info * VARIANT = $(VARIANT))
    $(info * BOARD   = $(BOARD))
    $(info * PROJECT = $(PROJECT))
    $(info -----------------------------)
endif

# -------------------------------------------------------------
#  Default targets
# -------------------------------------------------------------

prepare:
.PHONY: prepare

OBJS = \
	   $(addprefix $(DIR_BIN_PROJECT)/,$(addsuffix .o,$(basename $(PROJECT_SRC)))) \
	   $(addprefix $(DIR_BIN_PLATFORM)/,$(addsuffix .o,$(basename $(PLATFORM_SRC))))

$(DIR_BIN_PROJECT)/%.o: $(PROJECT_ROOT)/%.c
	@echo $(MSG_CC)
	$(Q)mkdir -p $(dir $@) $(DIR_BIN_DEPS)/$(dir $*)
	$(Q)$(CC) -c $(CPPFLAGS) $(CFLAGS) -o $@ $<

$(DIR_BIN_PROJECT)/%.o: $(PROJECT_ROOT)/%.cpp
	@echo $(MSG_CXX)
	$(Q)mkdir -p $(dir $@) $(DIR_BIN_DEPS)/$(dir $*)
	$(Q)$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) -o $@ $<

$(DIR_BIN_PROJECT)/%.o: $(PROJECT_ROOT)/%.s
	@echo $(MSG_AS)
	$(Q)mkdir -p $(dir $@) $(DIR_BIN_DEPS)/$(dir $*)
	$(Q)$(AS) $(ASFLAGS) -o $@ $<

$(DIR_BIN_PLATFORM)/%.o: $(PLATFORM_ROOT)/%.c
	@echo $(MSG_CC_PLATFORM)
	$(Q)mkdir -p $(dir $@) $(DIR_BIN_DEPS)/$(dir $*)
	$(Q)$(CC) -c $(CPPFLAGS) $(CCFLAGS) -o $@ $<

$(DIR_BIN_PLATFORM)/%.o: $(PLATFORM_ROOT)/%.s 
	@echo $(MSG_AS_PLATFORM)
	$(Q)mkdir -p $(dir $@)
	$(Q)$(AS) $(ASFLAGS) -o $@ $<

$(ELF_IMAGE): $(DIR_BIN) $(OBJS) $(LINKER_SCRIPT)
	@echo $(MSG_LD)
	$(Q)$(LD) $(ELF_LD_FLAGS) $(OBJS) -o $@ $(EXTRA_LD_FLAGS)

$(FLASH_SYMS): $(DIR_BIN) $(ELF_IMAGE)
	@echo $(MSG_SYMS)
	$(Q)$(NM) -n $< > $@

$(FLASH_IMAGE): $(ELF_IMAGE)
	@echo $(MSG_OBJCOPY)
	$(Q)$(OBJCOPY) -O binary $< $@

ifneq ($(wildcard $(DIR_BIN_DEPS)),)
include $(shell find $(DIR_BIN_DEPS) -type f)
endif


ifeq (,$(filter $(BOARD),$(COMPATIBLE_BOARDS)))
    $(error board is not compatible with project -- BOARD = $(BOARD), COMPATIBLE_BOARDS = $(COMPATIBLE_BOARDS))
endif


all: $(FLASH_IMAGE)
