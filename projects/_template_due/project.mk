
COMPATIBLE_BOARDS = sam3x8e

INCLUDES += \
		   $(PROJECT_ROOT)/include \
      $(PLATFORM_ROOT)/common/boards                                      \
      $(PLATFORM_ROOT)/common/services/clock                              \
      $(PLATFORM_ROOT)/common/services/gpio                               \
      $(PLATFORM_ROOT)/common/services/ioport                             \
      $(PLATFORM_ROOT)/common/services/serial                             \
      $(PLATFORM_ROOT)/common/services/serial/sam_uart                    \
      $(PLATFORM_ROOT)/common/utils                                       \
      $(PLATFORM_ROOT)/common/utils/stdio/stdio_serial                    \
      $(PLATFORM_ROOT)/sam/applications/getting-started                   \
      $(PLATFORM_ROOT)/sam/applications/getting-started/sam3x8e_arduino_due_x \
      $(PLATFORM_ROOT)/sam/boards                                         \
      $(PLATFORM_ROOT)/sam/boards/arduino_due_x                           \
      $(PLATFORM_ROOT)/sam/drivers/pio                                    \
      $(PLATFORM_ROOT)/sam/drivers/pmc                                    \
      $(PLATFORM_ROOT)/sam/drivers/tc                                     \
      $(PLATFORM_ROOT)/sam/drivers/uart                                   \
      $(PLATFORM_ROOT)/sam/drivers/usart                                  \
      $(PLATFORM_ROOT)/sam/utils                                          \
      $(PLATFORM_ROOT)/sam/utils/cmsis/sam3x/include                      \
      $(PLATFORM_ROOT)/sam/utils/cmsis/sam3x/source/templates             \
      $(PLATFORM_ROOT)/sam/utils/header_files                             \
      $(PLATFORM_ROOT)/sam/utils/preprocessor                             \
      $(PLATFORM_ROOT)/thirdparty/CMSIS/Include                           \
      $(PLATFORM_ROOT)/thirdparty/CMSIS/Lib/GCC

# TODO: extract variant- and board-related includes to make/*.mk files

PROJECT_SRC += \
			src/main.c

PLATFORM_SRC += \
       common/services/serial/usart_serial.c              \
       common/utils/interrupt/interrupt_sam_nvic.c        \
       common/utils/stdio/read.c                          \
       common/utils/stdio/write.c                         \
       sam/drivers/pio/pio.c                              \
       sam/drivers/pio/pio_handler.c                      \
       sam/drivers/pmc/pmc.c                              \
       sam/drivers/pmc/sleep.c                            \
       sam/drivers/tc/tc.c                                \
       sam/drivers/uart/uart.c                            \
       sam/drivers/usart/usart.c                          
	
ELF_LD_FLAGS += \
				-nostdlib -nodefaultlibs
