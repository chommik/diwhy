diwhy
=====

Simple build system for embedded microcontrollers.  
The story behind is, that I got mad at Arduino IDE's code generatation and overall
lack of functionality, while at the same time I didn't want to repeat the same commands 
all over. I also wanted to have a lighter environment than STM32CubeMX, which, even though has all the things needed, is quite bloated.


[[_TOC_]]

-------------------------------------------------------------------------------------------------

Available targets
-----------------

* `arm-none-eabi`, using GCC
* `arm-none-eabi-clang`, using Clang


Supported architectures and boards
----------------------------------

* **ARMv7-M**
    * **STM32**:
        * CPUs: at least STM32F1xx
        * support via [libopencm3](http://libopencm3.org/)
        * boards:
            * `maple_mini`, also sold as _blue pill_

    * **Atmel** SAM3X8E
        * partial support
        * support via [ASF][asf]
        * boards:
            * `sam3x8e`, Arduino Due

Planned:

* **Atmel** AVR


Compiling a project
-------------------

Required steps:

* get toolchain
* prepare dependencies
* compile


### Get toolchain

There are two ways to get toolchain.

**Prebuilt toolchain**:

```sh
# Clang:
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=arm-none-eabi-clang prebuilt-toolchain TOOLCHAIN_ARCHIVE=$(pwd)/dist/LLVMEmbeddedToolchainForArm-0.1-linux64.tar.gz

# GCC:
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=arm-none-eabi prebuilt-toolchain TOOLCHAIN_ARCHIVE=$(pwd)/dist/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2
```

**Build from source**:

```sh
# Clang:
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=arm-none-eabi-clang build-toolchain

# GCC:
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=arm-none-eabi build-toolchain
```

### Prepare dependencies

```sh
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=your_target prepare
```

### Compile

```sh
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=your_target flash
```

Support for `compile_commands.json`
-----------------------------------

The easiest way is to clean the build directory, prepare the toolchain and use [Bear][bear], for example:

```sh
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=arm-none-eabi-clang prebuilt-toolchain TOOLCHAIN_ARCHIVE=$(pwd)/dist/LLVMEmbeddedToolchainForArm-0.1-linux64.tar.gz
make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=your_target prepare
bear -- make VARIANT=stm32 BOARD=some_board PROJECT=some_project TARGET=your_target flash
```


Software used
-------------

### Atmel Advanced Software Framework, aka ASF

<https://asf.microchip.com/docs/latest/index.html>


### libopencm3

<https://github.com/libopencm3/libopencm3>, licenced under LGPLv3


### GNU Arm Embedded Toolchain

<https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads>


### LLVM-embedded-toolchain-for-Arm

<https://github.com/ARM-software/LLVM-embedded-toolchain-for-Arm>, licenced under Apache-2.0


[asf]: https://www.microchip.com/en-us/development-tools-tools-and-software/libraries-code-examples-and-more/advanced-software-framework-for-sam-devices
[bear]: https://github.com/rizsotto/Bear