
# -------------------------------------------------------------
#  Enable warnings
# -------------------------------------------------------------

WARNINGS_FLAGS = -Wall -Werror -pedantic \
                 -Werror=implicit-function-declaration

CFLAGS += $(WARNINGS_FLAGS)
CXXFLAGS += $(WARNINGS_FLAGS)

# -------------------------------------------------------------
#  Generate debug symbols
# -------------------------------------------------------------

CFLAGS += -g3
CXXFLAGS += -g3

# -------------------------------------------------------------
#  Generate debug symbols
# -------------------------------------------------------------

DEPS_FLAGS = -MT $@ -MMD -MP -MF $(DIR_BIN_DEPS)/$*.d

CPPFLAGS += $(DEPS_FLAGS) $(SYSTEM_CPPFLAGS)
LDFLAGS += $(DEPS_FLAGS)

# -------------------------------------------------------------
#  Linker flags
# -------------------------------------------------------------

LDFLAGS += \
           -L$(dir $(LINKER_SCRIPT)) \
           -Wl,-T,$(notdir $(LINKER_SCRIPT)) \
           -Wl,--gc-sections \
           -Wl,-Map=$(DIR_OUT)/$(PROJECT).map

ELF_LD_FLAGS = \
               $(LDFLAGS)


# -------------------------------------------------------------
#  Add include paths
# -------------------------------------------------------------

CPPFLAGS += $(addprefix -I,$(INCLUDES))
